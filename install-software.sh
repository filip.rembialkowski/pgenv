#!/bin/bash

echo "### Installing software"
apt-get -y update
#apt-get -y upgrade
apt-get -y install \
	postgresql-12 postgresql-client-12 \
	python3 python-is-python3 \
	python3-django \
	python3-psycopg2 \
	bash-completion lsof tmux vim rsync patch

echo "### Creating database"
sudo -iu postgres psql <<"EOF"
CREATE USER app01 WITH PASSWORD 'app01';
CREATE DATABASE app01 WITH OWNER app01;
EOF
