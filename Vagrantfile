# -*- mode: ruby -*-
# vi: set ft=ruby :

basedir = '/opt/django'
project = 'myproject'
app = 'myapp'
repo = 'https://gitlab.com/filip.rembialkowski/coding01.git'
#repo = 'git@gitlab.com:filip.rembialkowski/coding01.git'

Vagrant.configure('2') do |config|
  config.vm.box = 'generic/ubuntu2004'
  config.vm.provider 'virtualbox' do |vb|
    vb.memory = 500
    vb.cpus = 1
  end
  config.ssh.insert_key = false
  config.vm.define 'srv' do |n|
	n.vm.hostname = 'srv'
	# 8000 is the default port of Django "manage.py runserver"
	n.vm.network "forwarded_port", guest: 8000, host: 8000

	# Install Django, Postgres etc.
    n.vm.provision 'shell', path: 'install-software.sh', name: 'install-software.sh'
	
	# vanilla bootstrap:
	# n.vm.provision 'shell', path: 'django-bootstrap.sh', name: 'django-bootstrap.sh', args: [basedir, project, app]
	# Git-based bootstrap:
	n.vm.provision 'shell', inline: 'git clone %s %s' % [repo, basedir]

	# Create physical model in db:
	n.vm.provision 'shell', 'inline': 'python %s/manage.py migrate' % [basedir]
	# Load initial data, including root login for the admin app:
	n.vm.provision 'shell', 'inline': 'python %s/manage.py loaddata initial' % [basedir]
	
	# Run Django dev server:
	n.vm.provision 'shell', path: 'django-runserver.sh', name: 'django-runserver.sh', args: [basedir, project], run: "always"
  end
end
